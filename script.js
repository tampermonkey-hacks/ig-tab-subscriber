
// ==UserScript==
// @name         iGTab Subscriber
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  'Subscribe' to iGTab for free
// @author       Val Blant
// @match        https://igtab.com
// @require      https://code.jquery.com/jquery-3.6.1.min.js
// @icon         https://www.google.com/s2/favicons?sz=64&domain=tampermonkey.net
// @grant        none
// ==/UserScript==
/* globals $ */
/* globals jQuery */


(function() {
    'use strict';

    $.noConflict();

    const POPUP_SELECTOR = 'div.fc-ab-root'

    var waitForPopup = setInterval(function() {
        console.log("Waiting for popup...")

        if (jQuery(POPUP_SELECTOR).length) {

            // Kill the popup and page overlay
            //
            jQuery(POPUP_SELECTOR).remove();

            // Re-enable scrolling
            //
            jQuery('body').removeAttr('style');

            // Exit
            //
            clearInterval(waitForPopup);
        }
    }, 500);

})();
