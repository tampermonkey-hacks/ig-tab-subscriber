# ig-tab-subscriber
Install this Tampermonkey script if you like using https://igtab.com/ , but don't want to see ads (so you are using an ad blocker) or subscribe.

The script simply removes the popup overlay and re-enables scrolling.
